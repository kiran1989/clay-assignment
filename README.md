## Deploy Application
#### Step 1
Created an ubuntu 22.04 instance in AWS
#### Step 2
Installed Docker for minikube `sudo apt-get install docker-ce docker-ce-cli containerd.io `
#### Step 3
Installed minikube on instance, i have followed the below doc
`https://minikube.sigs.k8s.io/docs/start/`
#### Step 4
Installed kubectl 
`https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/`
#### Step 5
Installed helm `https://helm.sh/docs/intro/install/`
#### Step 6 
Deployed an app using helm and adjust the values.yaml. Below are some commands that I have used
```
helm create clay
```
#### Step 7
Created a Docker login and used it as an image pull secret for minikube `cat .dockerconfigjson | base64`, then I created a secret from `dockerconfigjson`. (This is not added in helm in order to avoid exposing your credentials, as this readme residing in my gitlab)
```
apiVersion: v1
kind: Secret
metadata:
  name: clay-gitlab-secret
  namespace: clay
type: kubernetes.io/dockerconfigjson
data:
  .dockerconfigjson: ewogICAgImF1dGhxxxxxxxxxxxxxxxxx
```
Created manifest and deployed the same to minikube in `clay` namespace.
```
kubectl create ns clay
helm template --release-name v1 --output-dir release clay-helm
kubectl apply -f release/v1/clay/templates/ -n clay-helm
deployment.apps/clay created
horizontalpodautoscaler.autoscaling/clay created
secret/clay-secrets created
service/clay-svc created
```
Verifed pod status using the command `kubectl get pods -n clay`
```
NAMESPACE     NAME                      READY   STATUS    RESTARTS      AGE
clay          clay-547fb546bd-bww7m     1/1     Running   0             7s
```
Verifed application that is exposed via nodeport.
```
NAMESPACE     NAME          TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)            AGE
clay          clay-svc      NodePort    10.99.45.201   <none>        8080:30008/TCP     16s
```
Created a HPA as well, you can verify using the command `kubectl get hpa -n clay`.
```
NAMESPACE   NAME   REFERENCE         TARGETS                        MINPODS   MAXPODS   REPLICAS   AGE
clay        clay   Deployment/clay   <unknown>/80%, <unknown>/80%   1         3         1          14h

```
#### Verified the application working on an exposed nodeport. However, since it runs on minikube, exposing it via a public IP is a bit tricky. Please use port forwarding to validate the same.
```
kubectl port-forward --address 0.0.0.0 svc/clay-svc 80:8080 -n clay
```
`http://ec2-13-200-235-117.ap-south-1.compute.amazonaws.com`

You will get the below homepage once application is running.

<img src="https://gitlab.com/kiran1989/clay-assignment/-/raw/main/index.png" width="800" />


## Feel free to login to the VM and verify the details and approach that I have taken.
```
username: ubuntu
Host: ec2-13-200-235-117.ap-south-1.compute.amazonaws.com
Key attached
```
# Part 2 POSTMAN
I performed a GET request using Postman, adding the header `'Accept: application/json'`, and received the expected 200 response.

<img src="https://gitlab.com/kiran1989/clay-assignment/-/raw/main/part-2.PNG" width="800" />

Enter access code

<img src="https://gitlab.com/kiran1989/clay-assignment/-/raw/main/part2-1.PNG" width="800" />

# Part 3 Find Secret.
I was able to locate the secret CODE from `interview-assignment.conf` after accessing the container. Do 
`kubectl exec -it clay-5b94b4856-sznmw /bin/sh -n clay` then `cat interview-assignment.conf`

<img src="https://gitlab.com/kiran1989/clay-assignment/-/raw/main/part-3.PNG" width="800" />

#### Now i am able to unlocked for next tasks.

<img src="https://gitlab.com/kiran1989/clay-assignment/-/raw/main/part3-1.PNG" width="800" />

# Task List
### 1 - Secret Handling
I have created a secret named `clay-secrets` with the key `code`. You can find it using `kubectl get secrets -n clay`. Then, I added a secret reference to an env variable in the deployment.yaml. 

For improved security its possible to use encryption like below.(Currently not used any). 
- https://kubernetes.io/docs/tasks/administer-cluster/encrypt-data/
- https://www.hashicorp.com/products/vault
```
          env:
            - name: CODE
              valueFrom:
                secretKeyRef:
                  name: clay-secrets
                  key: code
```
The `CODE` is now available as an environment variable inside the pod.

#### *You can now login with new code* `YesiReallywantThisJob`

<img src="https://gitlab.com/kiran1989/clay-assignment/-/raw/main/part4-11.PNG"/>

*After using the environment variable `CODE`,`http://ec2-13-200-235-117.ap-south-1.compute.amazonaws.com/secret` the login works with the new CODE. However, even after logging in, if you try to access the same URL in incognito mode, it does not prompt for the CODE again; it gets bypassed, and the login succeeds. This behavior seems unusual.*

### 2 - Application Lifecycle
Incorporate the healthcheck endpoint in your deployment. Added Liveness and rediness probes in values.yaml
```
livenessProbe:
  httpGet:
    path: /health
    port: 8080
  periodSeconds: 5
readinessProbe:
  httpGet:
    path: /health
    port: 8080
  periodSeconds: 5
```
Verified health check path, please refer the images.

<img src="https://gitlab.com/kiran1989/clay-assignment/-/raw/main/part4-2.PNG"/>

<img src="https://gitlab.com/kiran1989/clay-assignment/-/raw/main/part4-21.PNG"/>

### /toggle_health
Ensure when the endpoint goes unhealthy the container is restarted. A toggle health check was performed, causing the pod to become unhealthy and restart automatically
```
NAMESPACE     NAME                               READY   STATUS    RESTARTS       AGE
clay          clay-757bb97f68-rdqkw              1/1     Running   1 (3s ago)     14m

```
Record the Kubernetes events in your write-up.
Since the liveness probe failed, minikube automatically restarted the pod.

<img src="https://gitlab.com/kiran1989/clay-assignment/-/raw/main/part4-20.PNG"/>

<img src="https://gitlab.com/kiran1989/clay-assignment/-/raw/main/part2-4.PNG"/>

<img src="https://gitlab.com/kiran1989/clay-assignment/-/raw/main/part4-22.PNG"/>

### 3 - Script: list devices online/offline per country
I can successfully receive responses from the specified API endpoint `/devices` using GET request.

<img src="https://gitlab.com/kiran1989/clay-assignment/-/raw/main/part4-3.PNG" width="800"/>

I can successfully receive responses from the specified API endpoint `/device/[id]` using GET request.

<img src="https://gitlab.com/kiran1989/clay-assignment/-/raw/main/part4-31.PNG" width="800"/>

#### Create Monitoring Script
Shows the number of online and offline devices per country.

I have written a python script named `check_status.py` that has `requests` modules. This will performs fetching the API endpoints and displays the final result.

<img src="https://gitlab.com/kiran1989/clay-assignment/-/raw/main/part33.PNG"  width="800"/>

References:
- https://stackoverflow.com/questions/69688056/how-to-sort-this-list-based-on-age
- https://stackoverflow.com/questions/287871/how-do-i-print-colored-text-to-the-terminal
- https://www.w3schools.com/python/python_dictionaries_loop.asp

### 4 - Script: list devices in a subnet
Create a list of Device Identifiers which are part of CIDR 10.7.50.0/23

Adjusted the script `check_status.py` to include the `ipaddress` modules. Now, it lists the IP addresses of the given CIDR block and i have implemented the necessary changes to display the final result.

```
SUBNET_RANGE = [str(ip) for ip in ipaddress.IPv4Network('10.7.50.0/23')]
```
<img src="https://gitlab.com/kiran1989/clay-assignment/-/raw/main/part441.PNG"  width="800"/>

References:
- https://stackoverflow.com/questions/1942160/python-3-create-a-list-of-possible-ip-addresses-from-a-cidr-notation
- https://www.geeksforgeeks.org/check-if-element-exists-in-list-in-python/

### 5 - Obscure network traffic
I have captured the network logs using the TCP dump `tcpdump port not '(22 or 53 or 123)'`

<img src="https://gitlab.com/kiran1989/clay-assignment/-/raw/main/part5.png" />

Regarding the specific findings as you have mentioned in the task (A backdoor has been left in the app. Some kind of sensitive information is being leaked over the network.)

 1. Performed the action `/toggle_health` to restart the container, as the secuirty issue reported only during startup. And found this.
 ```
 15:46:35.108040 IP 147.161.133.35.21886 > ip-172-31-14-217.ap-south-1.compute.internal.http: Flags [P.], seq 1:537, ack 1, win 2081, options [nop,nop,TS val 17239524 ecr 3909138927], length 536: HTTP: GET /toggle_health HTTP/1.1
 ```
 2. Looks like there is a POST request being made from conatiner towards the endpoint `http://20.50.210.72/data`
 ```
 15:46:46.885765 IP ip-172-31-14-217.ap-south-1.compute.internal.39412 > 20.50.210.72.http: Flags [P.], seq 1:183, ack 1, win 502, options [nop,nop,TS val 1033606009 ecr 2013360902], length 182: HTTP: POST /data HTTP/1.1
 ```
 3. I attempted a reverse lookup of the IP to DNS using mxtoolbox.com, and it appears that the IP is associated with Azure. This suggests that the IP may indeed be related to the suspected backdoor.

 <img src="https://gitlab.com/kiran1989/clay-assignment/-/raw/main/part51.PNG"/>

4. I enabled more detailed logging using tcpdump with the command `tcpdump port not '(22 or 53 or 123)' -X` and eventually, I identified the leaked data and its destination. It turns out that the leaked data is my CODE `YesiReallywantThisJob`.

<img src="https://gitlab.com/kiran1989/clay-assignment/-/raw/main/part52.png"/>


## :)