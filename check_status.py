import requests as r   
import ipaddress

CRED = '\033[91m'
CGRN = '\33[32m'
CEND = '\033[0m'

SUBNET_RANGE = [str(ip) for ip in ipaddress.IPv4Network('10.7.50.0/23')]
DEVICE_ID_URL = "http://ec2-13-200-235-117.ap-south-1.compute.amazonaws.com/devices" 
DEVICE_ID_LIST = []
STATUS_LIST_DICT = []
COUNTRY_LIST = []

def get_device_ids():
    DEVICE_ID_LIST.clear()
    res = r.get(DEVICE_ID_URL)
    for x in res.json():
        JOIN = "http://ec2-13-200-235-117.ap-south-1.compute.amazonaws.com/device/"+x+""
        DEVICE_ID_LIST.append(JOIN)   # create url 
    return res.json()

get_device_ids()

for x in DEVICE_ID_LIST:
    res = r.get(x)
    STATUS_LIST_DICT.append(res.json()) # all status of devices
    COUNTRY_LIST.append((res.json().get('Location'),res.json())) # To find out what countries you have
    #print(f"calling: {res.json()}")

STATUS_LIST_DICT = sorted(STATUS_LIST_DICT, key=lambda x: x['Status']) # sorted listmap
STATUS_LIST_DICT = sorted(STATUS_LIST_DICT, key=lambda x: x['Location']) # sorted listmap

print("\nLIST DEVICE STATUS\n")
for x in STATUS_LIST_DICT:
    if x['Status'] == "offline":
        print(f"{CRED} Device ID: {x['Id']} | Location: {x['Location']} | Status: {x['Status']} {CEND}")
    elif x['Status'] == "online":
        print(f"{CGRN} Device ID: {x['Id']} | Location: {x['Location']} | Status: {x['Status']} {CEND}")

print("\nLIST Scheduled Maintenance IP\n")
for x in STATUS_LIST_DICT:
    if x['Ip'] in SUBNET_RANGE:
        MAINTENANCE = "Scheduled Maintenance"
        print(f"{CRED} Device ID: {x['Id']} | Location: {x['Location']} | Status: {x['Status']} | {MAINTENANCE} | Ip: {x['Ip']} {CEND}")

#COUNTRY_LIST = list(dict.fromkeys(COUNTRY_LIST))  # unique country list